@extends('layouts.app')

@section('content')

<div class="container">
    @forelse ($posts as $post)
    <div class="row">
        <div class="post__text col-6 offset-3">

            <a class="post__text--user mb-4" href="{{route('profile.show',['user' => $post->user])}}">
                <img src="{{$post->user->profile->profileImage()}}" alt="" class="img-fluid">
                <p class="text-center">{{$post->user->username}}</p>
            </a>
            <hr class="mt-0">
            <div class=" post__image">
                <img class="w-100" src="{{$post->image}}" alt="">
                <p class="post__text--caption mt-2"> <a href="{{route('profile.show',['user' => $post->user])}}">{{$post->user->username}}</a> {{$post->caption}}</p>
            </div>
        </div>
    </div>
    @empty
        <p>chale no sigues a nadie</p>
    @endforelse
    <div class="row">
        <div class="col-6 offset-3 d-flex justify-content-center">
            {{$posts->links()}}
        </div>
    </div>
</div>

@endsection
