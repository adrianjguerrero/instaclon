@extends('layouts.app')

@section('content')
<div class="container">
    <form class="row" action="/posts" enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
        <div class="col-8 offset-2">
            <div class="form-group row">
                <label for="caption" class="col-form-label text-md-right">Post caption</label>
                    <input id="caption" type="text" class="form-control @error('caption') is-invalid @enderror" name="caption" value="{{ old('caption') }}"  autocomplete="caption" autofocus>
                    @error('caption')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>
            <div class="form-group row">
                <label for="image" class="col-form-label text-md-right">Post image</label>
                    <input id="image" type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}"  autocomplete="image" autofocus>
                    @error('image')
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>

            <div class="form-group row">
                <input class="btn btn-primary" type="submit" value="Postear">
            </div>
        </div>
    </form>
</div>
@endsection
