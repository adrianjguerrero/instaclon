@extends('layouts.app')

@section('content')
<div class="container card">
    <div class="row post p-3">
        <div class="col-6 post__image">
            <img class="w-100" src="{{$post->image}}" alt="">
        </div>
        <div class="post__text col-6">
            <a class="post__text--user" href="{{route('profile.show',['user' => $post->user])}}">
                <img src="{{$post->user->profile->profileImage()}}" alt="" class="img-fluid">
                <p class="text-center">{{$post->user->username}}</p>
            </a>
            <a class="post__text--follow" href="#">Seguir</a>
            <hr>
            <p class="post__text--caption"> <a href="{{route('profile.show',['user' => $post->user])}}">{{$post->user->username}}</a> {{$post->caption}}</p>
        </div>
    </div>
</div>
@endsection
