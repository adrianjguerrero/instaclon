@extends('layouts.app')

@section('content')
<div class="container">

    <form class="row" action="/profile/{{$user->id}}" enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
        @method('PUT')
        <div class="col-8 offset-2">
            <h1>Editar perfil</h1>
                <div class="form-group row">
                    <label for="title" class="col-form-label text-md-right">Profile title</label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$user->profile->title ?? old('title')}}"  autocomplete="title" autofocus>
                        @error('title')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group row">
                    <label for="description" class="col-form-label text-md-right">Profile description</label>
                        <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $user->profile->description ?? old('description') }}"  autocomplete="description">
                        @error('description')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="url" class="col-form-label text-md-right">Profile url</label>
                        <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ $user->profile->url ?? old('url') }}"  autocomplete="url"  placeholder="http://sitio.com">
                        @error('url')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="image" class="col-form-label text-md-right">Profile image</label>
                        <input id="image" type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}"  autocomplete="image">
                        @error('image')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <input class="btn btn-primary" type="submit" value="Actualizar">
                </div>
            </div>
        </form>

</div>
@endsection
