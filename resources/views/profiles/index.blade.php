@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">

        <div class="col-3 p-5 user__img">
            <img class="rounded-circle img-fluid" src="{{$user->profile->profileImage()}}" alt="">
        </div>
        <div class="col-9 user__text">
          <div class="row">
          <h1 class="mr-3">{{$user->username}}</h1>
          @can('update', $user->profile)

          <a href="{{route('posts.create')}}" class="text-white btn btn-primary">Nuevo post</a>
          @endcan
          @if(Auth::check() && Auth::user()->id != $user->id)
          {{-- {{dd($follows)}} --}}
            <follow-button data-user-id="{{$user->id}}" data-follows="{{$follows}}"></follow-button>
          @endif
          </div>
          <div class="row">
              <p class="mr-2"><b class="mr-1">{{$postCount}}</b>posts</p>
              <p class="mr-2"><b class="mr-1">{{$followersCount}}</b>seguidores</p>
              <p class="mr-2"><b class="mr-1">{{$followingCount}}</b>siguiendo</p>
          </div>

          <div class="row">
              <b>{{optional($user->profile)->title}}</b>
          </div>
          <div class="row">
            {{optional($user->profile)->description}}
          </div>
          @if($user->profile)
            <div class="row">
                <b><a target="_blank" href="{{$user->profile->url ?? '#'}}">{{$user->profile->url ?? 'N/A'}}</a></b>
            </div>
          @endif
          {{-- @if(Auth::check() && Auth::user()->id == $user->id) --}}
          @can('update', $user->profile)

          <div class="row">

              <a href="{{route('profile.edit',['user' => $user])}}" class="btn btn-info text-white">Editar perfil</a>
          </div>
          @endcan
          {{-- @endif --}}
        </div>
    </div>

    <div class="row pt-5">
        @forelse ($user->posts as $post)

        <a href="{{route('posts.show',['post' => $post])}}" class="col-4 mb-3">
            <img class="w-100" src="{{$post->image}}" alt="">
        </a>
        @empty
            <p>Este usuario no ha subido nada</p>
        @endforelse
    </div>
  </div>
</div>
@endsection
