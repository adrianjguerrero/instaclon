<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yish\Imgur\Facades\Upload as Imgur;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    public function index(User $user)
    {

        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        // esta el usuario logeado? ok entonces ver si el logeado en la relacion de follow esta el id
        // si esta dara true, si no, false
        // y ps si no hay logeo mandar false



        $postCount = Cache::remember('count.posts'.$user->id,
         now()->addSeconds(30),
        function () use ($user) {
            return $user->posts->count();
        });
        // toda la paja de arriba es para cachear mierda
        // lo fino de cachear es no consultar la bd a cada rato
        // pero, esto esta puesto a q se venca a ls 30segs, en situaciones reales
        // se pone mas duh

        $followersCount = Cache::remember('count.followers'.$user->id,
         now()->addSeconds(30),
        function () use ($user) {
            return $user->profile->followers()->count();
        });

        $followingCount = Cache::remember('count.following'.$user->id,
         now()->addSeconds(30),
        function () use ($user) {
            return $user->following()->count();
        });

        return view('profiles.index',compact('user','follows','postCount','followersCount','followingCount'));
    }

    public function edit(User $user)
    {
        $this->authorize('update',$user->profile);

        return view('profiles.edit',['user' => $user]);
    }

    public function update(User $user)
    {

        $this->authorize('update',$user->profile);

        $data = request()->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'url' => 'url',
            'image' => 'image'
        ]);


        if(request('image')){


            if(env('APP_ENV') == 'production'){
                $image = request('image');


                $imagePath = Imgur::upload($image)->link();

            } else if(env('APP_ENV') == 'local'){


                $imagePath = '/storage/'.request('image')->store('profile','public');


                $image = Image::make(public_path($imagePath))->fit(1000,1000);
                $image->save();
            }
            $data['image'] = $imagePath;

        }


        // dd($data);

        auth()->user()->profile->update($data);
        // dd(auth()->user()->id);

        return redirect()->route('profile.show',['user' => auth()->user()->id]);
    }
}
