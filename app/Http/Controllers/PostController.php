<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Yish\Imgur\Facades\Upload as Imgur;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        // esto ayuda a q todos los metodos de aqui, requieran autenticacion
    }

    public function index()
    {
        $users = auth()->user()->following()->pluck('profiles.user_id');
        // del logeado, los q sigue, traler el user_id q esta en la tabla profile

        $posts = Post::whereIn('user_id',$users)->with('user')->latest()->paginate(5);
        // con el with hacemos consultas mas eficientes y asi take a better care about the server

        return view('posts.index',compact('posts'));
    }
    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $data = request()->validate([
            'caption' => 'required|string',
            'image' => 'required|image'
        ]);





        // para guardar la imagen en storage


        if(env('APP_ENV') == 'production'){
            $image = request('image');


            $imagePath = Imgur::upload($image)->link();

        } else if(env('APP_ENV') == 'local'){

            $imagePath = request('image')->store('uploads','public');

            $imagePath = "/storage/$imagePath";

            $image = Image::make(public_path("$imagePath"))->fit(1200,1200);
            $image->save();
        }



        // dd($image instanceof UploadedFile); esa imagen q subimos ya es una instanciacion

            // dd($image);

        $data['image'] = $imagePath;

        auth()->user()->posts()->create($data);
        //automaticamente asociara el autenticado con el post

        // Post::create($data);

        // dd(auth()->user);

        return redirect()->route('profile.show',['user' => auth()->user()]);
    }

    public function show(Post $post)
    {
        return view('posts.show', ['post' => $post]);
    }
}
