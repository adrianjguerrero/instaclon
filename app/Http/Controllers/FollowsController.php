<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    public function store(User $user)
    {
        // toggle: alternar, si lo tiene, q no lo tenga Y VICEVERSA

        // DAMN Q FACIL, AL LLAMAR, CREA LA RELACION, AL DARLE OTRA VEZ
        // LA BORRA OMG
        return auth()->user()->following()->toggle($user->profile);
    }
}
