<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(User::class)->create([
            'name' => 'Jhon Doe',
            'username' => 'jd',
            'email' => 'test@mail.com',
            'password' => bcrypt('password')
        ]);

        factory(User::class)->create([
            'name' => 'Dhon Joe',
            'username' => 'dj',
            'email' => 'mail@test.com',
            'password' => bcrypt('password')
        ]);

        // factory(User::class,14)->create();


    }
}
